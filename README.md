# Github Dashboard

A simple but powerful GitHub DashBoard.

## User page

![User page](pictures/picture1.PNG "User page")

## Repositories page

![Repositories page](pictures/picture2.PNG "Repositories page")

## Followers page

![Followers page](pictures/picture3.PNG "Followers page")

