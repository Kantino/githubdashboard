import 'dart:convert';

import 'package:github_dashboard/model/github_follower.dart';
import 'package:github_dashboard/model/github_repository.dart';
import 'package:github_dashboard/model/github_user.dart';
import 'package:http/http.dart';

const String _userEndPoint = "api.github.com";

/// Return the corresponding [GitHubUser] from [userName] if it exists or null otherwise.
/// Return null if it fails.
Future<GitHubUser?> getGitHubUser(String userName) async {
  Uri uri = Uri.https(_userEndPoint, "/users/$userName");
  try {
    Response res = await get(uri);
    if (res.statusCode == 200) {
      dynamic body = jsonDecode(res.body);
      return GitHubUser.fromJson(body);
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

/// Return the corresponding List of [GitHubRepository] from [userName] if it exists or null otherwise.
/// Return null if it fails.
Future<List<GitHubRepository>?> getUserRepositories(String userName) async {
  Uri uri = Uri.https(_userEndPoint, "/users/$userName/repos");
  try {
    Response res = await get(uri);
    if (res.statusCode == 200) {
      dynamic body = jsonDecode(res.body);
      List<GitHubRepository> repositories = [];
      for (var repositoryJson in body) {
        var repository = GitHubRepository.fromJson(repositoryJson);
        if (repository != null) {
          repositories.add(repository);
        }
      }
      return repositories;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

/// Return the corresponding List of [GitHubFollower] from [userName] if it exists or null otherwise.
/// Return null if it fails.
Future<List<GitHubFollower>?> getUserFollowers(String userName) async {
  Uri uri = Uri.https(_userEndPoint, "/users/$userName/followers");
  try {
    Response res = await get(uri);
    if (res.statusCode == 200) {
      dynamic body = jsonDecode(res.body);
      List<GitHubFollower> followers = [];
      for (var repositoryJson in body) {
        var follower = GitHubFollower.fromJson(repositoryJson);
        if (follower != null) {
          followers.add(follower);
        }
      }
      return followers;
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}
