import 'package:flutter/material.dart';

import 'view/pages/followers_page.dart';
import 'view/pages/repositories_page.dart';
import 'view/pages/user_page.dart';
import 'view/widgets/others/bottom_navigation_bar_widget.dart';

void main() {
  runApp(const GitHubDashBoardApp());
}

/// Entry point of the application
class GitHubDashBoardApp extends StatelessWidget {
  const GitHubDashBoardApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'GithHub DashBoard',
      debugShowCheckedModeBanner: false,
      scrollBehavior: const ScrollBehavior().copyWith(scrollbars: false),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const GitHubDashBoardWidget(title: 'GithHub DashBoard'),
    );
  }
}

/// Main widget of the application. It handles all the pages.
class GitHubDashBoardWidget extends StatefulWidget {
  const GitHubDashBoardWidget({super.key, required this.title});

  final String title;

  @override
  State<GitHubDashBoardWidget> createState() => _GitHubDashBoardWidgetState();
}

class _GitHubDashBoardWidgetState extends State<GitHubDashBoardWidget> {
  Widget page = const UserPage();

  /// Handle page selection based on [index]
  void selectPage(int index) {
    setState(() {
      switch (index) {
        case 0:
          page = const UserPage();
          break;
        case 1:
          page = const RepositoriesPage();
          break;
        case 2:
          page = const FollowersPage();
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        bottomNavigationBar: BottomNavigationBarWidget(
            onIndexChange: (index) => selectPage(index)),
        body: page);
  }
}
