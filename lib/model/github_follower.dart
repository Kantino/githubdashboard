class GitHubFollower {
  final String userName;
  final String? avatarUrl;

  GitHubFollower({required this.userName, required this.avatarUrl});

  static GitHubFollower? fromJson(dynamic jsonBody) {
    try {
      return GitHubFollower(
          userName: jsonBody["login"], avatarUrl: jsonBody["avatar_url"]);
    } catch (e) {
      return null;
    }
  }
}
