class GitHubUser {
  final String name;
  final String userName;
  final String? avatarUrl;
  final String? location;
  final String? bio;
  final int followersCount;
  final int repositoriesCount;

  GitHubUser(
      {required this.name,
      required this.userName,
      required this.avatarUrl,
      required this.location,
      required this.bio,
      required this.followersCount,
      required this.repositoriesCount});

  static GitHubUser? fromJson(dynamic jsonBody) {
    try {
      return GitHubUser(
          name: jsonBody["name"],
          userName: jsonBody["login"],
          avatarUrl: jsonBody["avatar_url"],
          location: jsonBody["location"],
          bio: jsonBody["bio"],
          followersCount: jsonBody["followers"],
          repositoriesCount: jsonBody["public_repos"]);
    } catch (e) {
      return null;
    }
  }
}
