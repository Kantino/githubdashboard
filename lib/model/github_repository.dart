class GitHubRepository {
  final String name;
  final String? description;
  final int stars;

  GitHubRepository(
      {required this.name, required this.description, required this.stars});

  static GitHubRepository? fromJson(dynamic jsonBody) {
    try {
      return GitHubRepository(
          name: jsonBody["name"],
          description: jsonBody["description"],
          stars: jsonBody["stargazers_count"]);
    } catch (e) {
      return null;
    }
  }
}
