import 'package:flutter/material.dart';
import 'package:github_dashboard/view/pages/basic_page.dart';
import 'package:github_dashboard/view/widgets/follower/followers_widget.dart';

/// A page that displays the followers of a given user.
class FollowersPage extends StatelessWidget {
  const FollowersPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicPage(
        title: "Followers",
        getChild: ((userName) => FollowersWidget(userName: userName)));
  }
}
