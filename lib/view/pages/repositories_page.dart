import 'package:flutter/material.dart';
import 'package:github_dashboard/view/pages/basic_page.dart';
import 'package:github_dashboard/view/widgets/repository/repositories_widget.dart';

/// A page that displays the repositories of a given user.
class RepositoriesPage extends StatelessWidget {
  const RepositoriesPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicPage(
        title: "Repositories",
        getChild: ((userName) => RepositoriesWidget(userName: userName)));
  }
}
