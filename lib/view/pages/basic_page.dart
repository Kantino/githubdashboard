import 'package:flutter/material.dart';
import 'package:github_dashboard/view/widgets/others/search_bar_widget.dart';

/// Basic page to display a user searchbar and a child that is updated with [getChild] method for every new search.
///
class BasicPage extends StatefulWidget {
  const BasicPage({super.key, required this.getChild, required this.title});

  final Widget Function(String userName) getChild;

  final String title;

  @override
  State<StatefulWidget> createState() => _BasicPage();
}

class _BasicPage extends State<BasicPage> {
  String userName = "";
  bool hasSearch = false;
  void onSubmit(String value) {
    setState(() {
      hasSearch = true;
      userName = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Text(widget.title,
          style: const TextStyle(fontSize: 26, fontWeight: FontWeight.bold)),
      SearchBarWidget(
        onSubmit: (value) => onSubmit(value),
        hintText: "Search user...",
      ),
      // We don't display anything before the first search.
      hasSearch ? widget.getChild(userName) : const SizedBox.shrink()
    ]);
  }
}
