import 'package:flutter/material.dart';
import 'package:github_dashboard/view/pages/basic_page.dart';
import 'package:github_dashboard/view/widgets/user/user_widget.dart';

/// A page that displays a given user data.
class UserPage extends StatelessWidget {
  const UserPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BasicPage(
        title: "Users",
        getChild: ((userName) => UserWidget(userName: userName)));
  }
}
