import 'package:flutter/material.dart';
import 'package:github_dashboard/model/github_follower.dart';

import '../../../http_services.dart';

/// Displays the followers of [userName]
class FollowersWidget extends StatelessWidget {
  const FollowersWidget({super.key, required this.userName});

  final String userName;

  @override
  Widget build(BuildContext context) {
    // Display the followers after getting them from the API
    return FutureBuilder<List<GitHubFollower>?>(
        future: getUserFollowers(userName),
        builder: (BuildContext context,
            AsyncSnapshot<List<GitHubFollower>?> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const CircularProgressIndicator();
          } else {
            if (!snapshot.hasData || snapshot.data == null) {
              return Text("No data found for user $userName");
            } else {
              var followers = snapshot.data!;
              return Expanded(
                  child: Column(mainAxisSize: MainAxisSize.min, children: [
                Text("$userName has ${followers.length} followers"),
                Expanded(
                    child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: followers.length,
                  itemBuilder: (context, index) {
                    var follower = followers[index];
                    double size = 50;
                    return ListTile(
                        leading: follower.avatarUrl != null &&
                                follower.avatarUrl!.isNotEmpty
                            ? Image.network(follower.avatarUrl!,
                                width: size, height: size)
                            // Display question mark in white circle if no avatar found
                            : Container(
                                width: size,
                                height: size,
                                decoration: const BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle,
                                ),
                                child: const Icon(Icons.question_mark)),
                        title: Text(follower.userName));
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(height: 10),
                ))
              ]));
            }
          }
        });
  }
}
