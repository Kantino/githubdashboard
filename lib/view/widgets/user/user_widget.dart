import 'package:flutter/material.dart';
import 'package:github_dashboard/model/github_user.dart';

import '../../../http_services.dart';

/// Display data for one [GitHubUser]
class UserWidget extends StatelessWidget {
  const UserWidget({super.key, required this.userName});

  final String userName;

  @override
  Widget build(BuildContext context) {
    // Display data after fetching the API
    return FutureBuilder<GitHubUser?>(
        future: getGitHubUser(userName),
        builder: (BuildContext context, AsyncSnapshot<GitHubUser?> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const CircularProgressIndicator();
          } else {
            if (!snapshot.hasData || snapshot.data == null) {
              return Text("No data found for user $userName");
            } else {
              var user = snapshot.data!;
              const double avatarSize = 100;
              return Expanded(
                  child: SingleChildScrollView(
                      child: SizedBox(
                width: double.infinity,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    user.avatarUrl != null && user.avatarUrl!.isNotEmpty
                        ? Image.network(user.avatarUrl!,
                            width: avatarSize, height: avatarSize)
                        : const SizedBox.shrink(),
                    Text(user.userName,
                        style: const TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 28)),
                    Text(user.name),
                    user.location != null && user.location!.isNotEmpty
                        ? Text(user.location!)
                        : const SizedBox.shrink(),
                    user.bio != null && user.bio!.isNotEmpty
                        ? Text(user.bio!)
                        : const SizedBox.shrink(),
                    Text("Repositories: ${user.repositoriesCount}"),
                    Text("Followers: ${user.followersCount}")
                  ],
                ),
              )));
            }
          }
        });
  }
}
