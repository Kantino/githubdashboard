import 'package:flutter/material.dart';

/// Bottom navigation bar with a callback [onIndexChange]
class BottomNavigationBarWidget extends StatefulWidget {
  const BottomNavigationBarWidget({Key? key, required this.onIndexChange})
      : super(key: key);

  final void Function(int index) onIndexChange;

  @override
  State<StatefulWidget> createState() => _BottomNavigationBarWidget();
}

class _BottomNavigationBarWidget extends State<BottomNavigationBarWidget> {
  final double fontSize = 14;
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: currentIndex,
      selectedFontSize: fontSize,
      unselectedFontSize: fontSize,
      onTap: (index) {
        if (index != currentIndex) {
          setState(() {
            currentIndex = index;
          });
          widget.onIndexChange(currentIndex);
        }
      },
      items: const [
        BottomNavigationBarItem(
            label: "Users", icon: Icon(Icons.account_circle)),
        BottomNavigationBarItem(
            label: "Repositories", icon: Icon(Icons.folder)),
        BottomNavigationBarItem(label: "Followers", icon: Icon(Icons.people)),
      ],
    );
  }
}
