import 'package:flutter/material.dart';

/// A search bar with a callback [onSubmit]
class SearchBarWidget extends StatefulWidget {
  const SearchBarWidget({super.key, required this.onSubmit, this.hintText});

  final void Function(String search) onSubmit;
  final String? hintText;

  @override
  State<StatefulWidget> createState() => _SearchBarWidget();
}

class _SearchBarWidget extends State<SearchBarWidget> {
  final TextEditingController textEditingController = TextEditingController();

  @override
  void dispose() {
    textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
        child: TextField(
          controller: textEditingController,
          decoration: InputDecoration(
              hintText: widget.hintText,
              suffixIcon: IconButton(
                onPressed: () {
                  widget.onSubmit(textEditingController.text);
                },
                icon: const Icon(Icons.search),
              )),
          onSubmitted: (value) => widget.onSubmit(textEditingController.text),
        ));
  }
}
