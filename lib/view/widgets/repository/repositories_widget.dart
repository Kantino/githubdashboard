import 'package:flutter/material.dart';
import 'package:github_dashboard/model/github_repository.dart';
import 'package:github_dashboard/view/widgets/repository/repositories_with_sort_options_widget.dart';

import '../../../http_services.dart';

/// Displays the repositories of [userName]
class RepositoriesWidget extends StatelessWidget {
  const RepositoriesWidget({super.key, required this.userName});

  final String userName;

  @override
  Widget build(BuildContext context) {
    // Display the repositorires after getting them from the API
    return FutureBuilder<List<GitHubRepository>?>(
        future: getUserRepositories(userName),
        builder: (BuildContext context,
            AsyncSnapshot<List<GitHubRepository>?> snapshot) {
          if (snapshot.connectionState != ConnectionState.done) {
            return const CircularProgressIndicator();
          } else {
            if (!snapshot.hasData || snapshot.data == null) {
              return Text("No data found for user $userName");
            } else {
              var repositories = snapshot.data!;
              return RepositoriesWithSortOptionsWidget(
                  userName: userName, repositories: repositories);
            }
          }
        });
  }
}
