import 'package:flutter/material.dart';
import 'package:github_dashboard/model/github_repository.dart';
import 'package:github_dashboard/view/widgets/repository/repository_widget.dart';

/// Display [repositories] and give options to sort them
class RepositoriesWithSortOptionsWidget extends StatefulWidget {
  const RepositoriesWithSortOptionsWidget(
      {super.key, required this.repositories, required this.userName});

  final String userName;

  final List<GitHubRepository> repositories;

  @override
  State<StatefulWidget> createState() => _RepositoriesWithSortOptionsWidget();
}

class _RepositoriesWithSortOptionsWidget
    extends State<RepositoriesWithSortOptionsWidget> {
  List<GitHubRepository> get repositories => widget.repositories;

  /// Sort repositories depending on stars and [ascending] boolean
  void sortRepositories(bool ascending) {
    setState(() {
      repositories.sort((a, b) =>
          ascending ? a.stars.compareTo(b.stars) : b.stars.compareTo(a.stars));
    });
  }

  @override
  Widget build(BuildContext context) {
    return repositories.isEmpty
        ? Text("${widget.userName} has no repository")
        : Expanded(
            child: Column(mainAxisSize: MainAxisSize.min, children: [
            Wrap(crossAxisAlignment: WrapCrossAlignment.center, children: [
              TextButton(
                  onPressed: () => sortRepositories(true),
                  child: const Text("Ascending")),
              TextButton(
                  onPressed: () => sortRepositories(false),
                  child: const Text("Descending"))
            ]),
            Expanded(
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: repositories.length,
                    itemBuilder: (context, index) => RepositoryWidget(
                        key: UniqueKey(), repository: repositories[index])))
          ]));
  }
}
