import 'package:flutter/material.dart';
import 'package:github_dashboard/model/github_repository.dart';

/// Display one [GitHubRepository]
class RepositoryWidget extends StatelessWidget {
  const RepositoryWidget({super.key, required this.repository});

  final GitHubRepository repository;

  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(repository.name),
        subtitle:
            repository.description != null && repository.description!.isNotEmpty
                ? Text(repository.description!)
                : null,
        trailing: Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          children: [Text(repository.stars.toString()), const Icon(Icons.star)],
        ));
  }
}
